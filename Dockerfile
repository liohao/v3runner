# 源码目录执行命令构建镜像 docker build -t cube-cms-admin .
# 运行镜像启动服务 docker run -p 3003:80 cube-cms-admin
# 构建阶段
FROM node:18-alpine AS builder
WORKDIR /build
COPY . .
# 服务器编译打包
# 根据服务器环境选择国内源
# 服务器资源不足的可以在本地打包上传dist目录到源码，从这里开始注释到构建阶段结束
RUN npm config set registry https://registry.npmmirror.com
# set -x 输出调试信息
RUN set -x \
    && npm install \
    && echo "Install Done." \
    && npm run build \
    && echo "Build Done."

# 第二阶段
# 选择必要文件，减少镜像尺寸
FROM nginx:stable-alpine as prod
COPY --from=builder /build/dist /www
COPY nginx.conf /etc/nginx/nginx.conf
# 暴露端口
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
# 调试
# CMD ["sh", "-c", "sleep 86400"]
